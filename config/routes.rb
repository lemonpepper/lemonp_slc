Rails.application.routes.draw do
  namespace(:admin){ resources :internal_images }
  namespace(:admin){ 
    resources :configuration_sites
    resources(:products) do
      resources :product_images
    end
    resources :fiber_types
    resources :roll_widths
    resources :height_fibreas
    resources :product_lines
    resources(:featureds) do
      member do 
        get :destroy_image
      end
    end 
    resources :newsletters
    resources :users
    root :to => 'products#index'
  }
  post '/tinymce_assets' => 'tinymce_assets#create'   
  post '/tinymce_documents' => 'tinymce_documents#create' 
  localized do
    resources :products, :only => [:show]
    resources :reselers, :only => [:new, :show, :create]
    resources :product_lines, :only => [:show]
    resources :user_sessions, :only => [:new, :create, :destroy] 
    
    get "/" => "homes#index", :as => "root"
    post "/" => "homes#index", :as => "root"
    match '/login' => 'user_sessions#new', via: :get  
    match '/logout' => 'user_sessions#destroy', via: :get
    match '/about' => 'homes#about', via: :get
    match '/synthetic_grass' => 'homes#synthetic_grass', via: :get
    match '/research_and_innovation' => 'homes#research_and_innovation', via: :get
    match '/warranty' => 'homes#warranty', via: :get
    match '/filters' => 'homes#filters', via: :post
    match '/filters' => 'homes#filters', via: :get

  end

end



# The priority is based upon order of creation: first created -> highest priority.
# See how all your routes lay out with "rake routes".

# You can have the root of your site routed with "root"
# root 'welcome#index'

# Example of regular route:
#   get 'products/:id' => 'catalog#view'

# Example of named route that can be invoked with purchase_url(id: product.id)
#   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

# Example resource route (maps HTTP verbs to controller actions automatically):
#   resources :products

# Example resource route with options:
#   resources :products do
#     member do
#       get 'short'
#       post 'toggle'
#     end
#
#     collection do
#       get 'sold'
#     end
#   end

# Example resource route with sub-resources:
#   resources :products do
#     resources :comments, :sales
#     resource :seller
#   end

# Example resource route with more complex sub-resources:
#   resources :products do
#     resources :comments
#     resources :sales do
#       get 'recent', on: :collection
#     end
#   end

# Example resource route with concerns:
#   concern :toggleable do
#     post 'toggle'
#   end
#   resources :posts, concerns: :toggleable
#   resources :photos, concerns: :toggleable

# Example resource route within a namespace:
#   namespace :admin do
#     # Directs /admin/products/* to Admin::ProductsController
#     # (app/controllers/admin/products_controller.rb)
#     resources :products
#   end