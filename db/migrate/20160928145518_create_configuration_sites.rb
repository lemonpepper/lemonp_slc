class CreateConfigurationSites < ActiveRecord::Migration
  def self.up
    create_table :configuration_sites do |t|
      t.string :title_institutional
      t.text :description_institutional
      t.string :movie_institutional
      t.text :movie_text_institutional
      t.string :title_research_innovation
      t.text :description_research_innovation
      t.string :title_warranty
      t.text :description_warranty
      t.string :link_facebook
      t.string :link_instagram
      t.string :email_contact
      t.boolean :active,:null => false, :default => 1
      t.boolean :published,:null => false, :default => 1
      t.timestamps
    end
  end

  def self.down
    drop_table :configuration_sites
  end
end
