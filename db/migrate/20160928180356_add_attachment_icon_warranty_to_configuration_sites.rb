class AddAttachmentIconWarrantyToConfigurationSites < ActiveRecord::Migration
  def self.up
    change_table :configuration_sites do |t|
      t.attachment :icon_warranty
    end
  end

  def self.down
    remove_attachment :configuration_sites, :icon_warranty
  end
end
