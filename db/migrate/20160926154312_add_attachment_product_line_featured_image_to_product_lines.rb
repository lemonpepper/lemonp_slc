class AddAttachmentProductLineFeaturedImageToProductLines < ActiveRecord::Migration
  def self.up
    change_table :product_lines do |t|
      t.attachment :product_line_featured_image
    end
  end

  def self.down
    remove_attachment :product_lines, :product_line_featured_image
  end
end
