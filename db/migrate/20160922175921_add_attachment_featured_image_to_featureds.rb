class AddAttachmentFeaturedImageToFeatureds < ActiveRecord::Migration
  def self.up
    change_table :featureds do |t|
      t.attachment :featured_image
    end
  end

  def self.down
    remove_attachment :featureds, :featured_image
  end
end
