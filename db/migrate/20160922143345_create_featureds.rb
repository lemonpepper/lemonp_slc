class CreateFeatureds < ActiveRecord::Migration
  def self.up
    create_table :featureds do |t|
      t.boolean :active,:null => false, :default => 1
      t.boolean :published,:null => false, :default => 1
      t.timestamps
    end
  end

  def self.down
    drop_table :featureds
  end
end
