class AddAttachmentCoverWarrantyToConfigurationSites < ActiveRecord::Migration
  def self.up
    change_table :configuration_sites do |t|
      t.attachment :cover_warranty
    end
  end

  def self.down
    remove_attachment :configuration_sites, :cover_warranty
  end
end
