class AddWhatsAppToConfigurationSite < ActiveRecord::Migration
  def change
    add_column :configuration_sites, :whatsapp, :string
  end
end
