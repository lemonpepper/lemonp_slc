class AddAttachmentCoverSyntheticGrassToConfigurationSites < ActiveRecord::Migration
  def self.up
    change_table :configuration_sites do |t|
      t.attachment :cover_synthetic_grass
    end
  end

  def self.down
    remove_attachment :configuration_sites, :cover_synthetic_grass
  end
end
