class AddSyntheticGrassToConfigurationSite < ActiveRecord::Migration
  def change
    add_column :configuration_sites, :title_synthetic_grass, :string
    add_column :configuration_sites, :description_synthetic_grass, :text
  end
end
