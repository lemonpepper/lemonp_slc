class AddMovieInstitutionalTwoToConfigurationSite < ActiveRecord::Migration
  def change
    add_column :configuration_sites, :movie_institutional_two, :string
  end
end
