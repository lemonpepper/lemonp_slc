class AddTitleAndOrderToFeatured < ActiveRecord::Migration
  def change
    add_column :featureds, :title, :string
    add_column :featureds, :order, :integer,:null => false, :default => 0
  end
end
