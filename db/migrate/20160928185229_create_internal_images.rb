class CreateInternalImages < ActiveRecord::Migration
  def self.up
    create_table :internal_images do |t|
      t.string :title
      t.integer :category
      t.boolean :active,:null => false, :default => 1
      t.boolean :published,:null => false, :default => 1
      t.timestamps
    end
  end

  def self.down
    drop_table :internal_images
  end
end
