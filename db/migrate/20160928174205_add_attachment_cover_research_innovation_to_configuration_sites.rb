class AddAttachmentCoverResearchInnovationToConfigurationSites < ActiveRecord::Migration
  def self.up
    change_table :configuration_sites do |t|
      t.attachment :cover_research_innovation
    end
  end

  def self.down
    remove_attachment :configuration_sites, :cover_research_innovation
  end
end
