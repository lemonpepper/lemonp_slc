class AddAttachmentImageProdToProductImages < ActiveRecord::Migration
  def self.up
    change_table :product_images do |t|
      t.attachment :image_prod
    end
  end

  def self.down
    remove_attachment :product_images, :image_prod
  end
end
