class AddLinkToFeatured < ActiveRecord::Migration
  def change
    add_column :featureds, :link, :string
  end
end
