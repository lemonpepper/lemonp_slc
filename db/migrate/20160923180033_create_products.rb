class CreateProducts < ActiveRecord::Migration
  def self.up
    create_table :products do |t|
      t.string :title
      t.text :description
      t.integer :product_line_id
      t.integer :height_fibrea_id
      t.integer :roll_width_id
      t.integer :fiber_type_id
      t.string :length_roll
      t.string :weight
      t.string :thickness_wire
      t.string :basis
      t.string :warranty
      t.boolean :active,:null => false, :default => 1
      t.boolean :published,:null => false, :default => 1
      t.timestamps
    end
  end

  def self.down
    drop_table :products
  end
end
