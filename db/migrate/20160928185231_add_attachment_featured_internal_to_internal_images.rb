class AddAttachmentFeaturedInternalToInternalImages < ActiveRecord::Migration
  def self.up
    change_table :internal_images do |t|
      t.attachment :featured_internal
    end
  end

  def self.down
    remove_attachment :internal_images, :featured_internal
  end
end
