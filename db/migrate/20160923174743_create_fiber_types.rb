class CreateFiberTypes < ActiveRecord::Migration
  def self.up
    create_table :fiber_types do |t|
      t.string :title
      t.boolean :active,:null => false, :default => 1
      t.boolean :published,:null => false, :default => 1
      t.timestamps
    end
  end

  def self.down
    drop_table :fiber_types
  end
end
