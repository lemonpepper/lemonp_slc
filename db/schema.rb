# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180324183305) do

  create_table "configuration_sites", force: :cascade do |t|
    t.string   "title_institutional",                    limit: 255
    t.text     "description_institutional",              limit: 65535
    t.string   "movie_institutional",                    limit: 255
    t.text     "movie_text_institutional",               limit: 65535
    t.string   "title_research_innovation",              limit: 255
    t.text     "description_research_innovation",        limit: 65535
    t.string   "title_warranty",                         limit: 255
    t.text     "description_warranty",                   limit: 65535
    t.string   "link_facebook",                          limit: 255
    t.string   "link_instagram",                         limit: 255
    t.string   "email_contact",                          limit: 255
    t.boolean  "active",                                               default: true, null: false
    t.boolean  "published",                                            default: true, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "cover_institutional_file_name",          limit: 255
    t.string   "cover_institutional_content_type",       limit: 255
    t.integer  "cover_institutional_file_size",          limit: 4
    t.datetime "cover_institutional_updated_at"
    t.string   "cover_research_innovation_file_name",    limit: 255
    t.string   "cover_research_innovation_content_type", limit: 255
    t.integer  "cover_research_innovation_file_size",    limit: 4
    t.datetime "cover_research_innovation_updated_at"
    t.string   "cover_warranty_file_name",               limit: 255
    t.string   "cover_warranty_content_type",            limit: 255
    t.integer  "cover_warranty_file_size",               limit: 4
    t.datetime "cover_warranty_updated_at"
    t.string   "icon_institutional_file_name",           limit: 255
    t.string   "icon_institutional_content_type",        limit: 255
    t.integer  "icon_institutional_file_size",           limit: 4
    t.datetime "icon_institutional_updated_at"
    t.string   "icon_research_innovation_file_name",     limit: 255
    t.string   "icon_research_innovation_content_type",  limit: 255
    t.integer  "icon_research_innovation_file_size",     limit: 4
    t.datetime "icon_research_innovation_updated_at"
    t.string   "icon_warranty_file_name",                limit: 255
    t.string   "icon_warranty_content_type",             limit: 255
    t.integer  "icon_warranty_file_size",                limit: 4
    t.datetime "icon_warranty_updated_at"
    t.string   "title_synthetic_grass",                  limit: 255
    t.text     "description_synthetic_grass",            limit: 65535
    t.string   "cover_synthetic_grass_file_name",        limit: 255
    t.string   "cover_synthetic_grass_content_type",     limit: 255
    t.integer  "cover_synthetic_grass_file_size",        limit: 4
    t.datetime "cover_synthetic_grass_updated_at"
    t.string   "icon_synthetic_grass_file_name",         limit: 255
    t.string   "icon_synthetic_grass_content_type",      limit: 255
    t.integer  "icon_synthetic_grass_file_size",         limit: 4
    t.datetime "icon_synthetic_grass_updated_at"
    t.string   "movie_institutional_two",                limit: 255
    t.string   "whatsapp",                               limit: 255
  end

  create_table "documents", force: :cascade do |t|
    t.string   "title",             limit: 255
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.string   "file_file_name",    limit: 255
    t.string   "file_content_type", limit: 255
    t.integer  "file_file_size",    limit: 4
    t.datetime "file_updated_at"
  end

  create_table "featureds", force: :cascade do |t|
    t.boolean  "active",                                  default: true, null: false
    t.boolean  "published",                               default: true, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "title",                       limit: 255
    t.integer  "order",                       limit: 4,   default: 0,    null: false
    t.string   "featured_image_file_name",    limit: 255
    t.string   "featured_image_content_type", limit: 255
    t.integer  "featured_image_file_size",    limit: 4
    t.datetime "featured_image_updated_at"
    t.string   "link",                        limit: 255
  end

  create_table "fiber_types", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.boolean  "active",                 default: true, null: false
    t.boolean  "published",              default: true, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "height_fibreas", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.boolean  "active",                 default: true, null: false
    t.boolean  "published",              default: true, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "images", force: :cascade do |t|
    t.string   "alt",               limit: 255, default: ""
    t.string   "hint",              limit: 255, default: ""
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "file_file_name",    limit: 255
    t.string   "file_content_type", limit: 255
    t.integer  "file_file_size",    limit: 4
    t.datetime "file_updated_at"
  end

  create_table "internal_images", force: :cascade do |t|
    t.string   "title",                          limit: 255
    t.integer  "category",                       limit: 4
    t.boolean  "active",                                     default: true, null: false
    t.boolean  "published",                                  default: true, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "featured_internal_file_name",    limit: 255
    t.string   "featured_internal_content_type", limit: 255
    t.integer  "featured_internal_file_size",    limit: 4
    t.datetime "featured_internal_updated_at"
  end

  create_table "newsletters", force: :cascade do |t|
    t.string   "email",      limit: 255
    t.boolean  "active",                 default: true, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "product_images", force: :cascade do |t|
    t.string   "title",                   limit: 255
    t.integer  "product_id",              limit: 4
    t.boolean  "active",                              default: true, null: false
    t.boolean  "published",                           default: true, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image_prod_file_name",    limit: 255
    t.string   "image_prod_content_type", limit: 255
    t.integer  "image_prod_file_size",    limit: 4
    t.datetime "image_prod_updated_at"
  end

  create_table "product_lines", force: :cascade do |t|
    t.string   "name",                                     limit: 255
    t.integer  "order",                                    limit: 4
    t.boolean  "active",                                               default: true, null: false
    t.boolean  "published",                                            default: true, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "product_line_image_file_name",             limit: 255
    t.string   "product_line_image_content_type",          limit: 255
    t.integer  "product_line_image_file_size",             limit: 4
    t.datetime "product_line_image_updated_at"
    t.string   "slug",                                     limit: 255
    t.string   "product_line_internal_image_file_name",    limit: 255
    t.string   "product_line_internal_image_content_type", limit: 255
    t.integer  "product_line_internal_image_file_size",    limit: 4
    t.datetime "product_line_internal_image_updated_at"
    t.string   "product_line_featured_image_file_name",    limit: 255
    t.string   "product_line_featured_image_content_type", limit: 255
    t.integer  "product_line_featured_image_file_size",    limit: 4
    t.datetime "product_line_featured_image_updated_at"
  end

  add_index "product_lines", ["slug"], name: "index_product_lines_on_slug", unique: true, using: :btree

  create_table "products", force: :cascade do |t|
    t.string   "title",                           limit: 255
    t.text     "description",                     limit: 65535
    t.integer  "product_line_id",                 limit: 4
    t.integer  "height_fibrea_id",                limit: 4
    t.integer  "roll_width_id",                   limit: 4
    t.integer  "fiber_type_id",                   limit: 4
    t.string   "length_roll",                     limit: 255
    t.string   "weight",                          limit: 255
    t.string   "thickness_wire",                  limit: 255
    t.string   "basis",                           limit: 255
    t.string   "warranty",                        limit: 255
    t.boolean  "active",                                        default: true, null: false
    t.boolean  "published",                                     default: true, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "product_home_image_file_name",    limit: 255
    t.string   "product_home_image_content_type", limit: 255
    t.integer  "product_home_image_file_size",    limit: 4
    t.datetime "product_home_image_updated_at"
    t.string   "slug",                            limit: 255
  end

  add_index "products", ["slug"], name: "index_products_on_slug", unique: true, using: :btree

  create_table "roles", force: :cascade do |t|
    t.string   "name",              limit: 255,                 null: false
    t.string   "authorizable_type", limit: 255
    t.integer  "authorizable_id",   limit: 4
    t.boolean  "system",                        default: false, null: false
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
  end

  add_index "roles", ["authorizable_type", "authorizable_id"], name: "index_roles_on_authorizable_type_and_authorizable_id", using: :btree
  add_index "roles", ["name"], name: "index_roles_on_name", using: :btree

  create_table "roles_users", id: false, force: :cascade do |t|
    t.integer "user_id", limit: 4, null: false
    t.integer "role_id", limit: 4, null: false
  end

  add_index "roles_users", ["role_id"], name: "index_roles_users_on_role_id", using: :btree
  add_index "roles_users", ["user_id"], name: "index_roles_users_on_user_id", using: :btree

  create_table "roll_widths", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.boolean  "active",                 default: true, null: false
    t.boolean  "published",              default: true, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sessions", force: :cascade do |t|
    t.string   "session_id", limit: 255,   null: false
    t.text     "data",       limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sessions", ["session_id"], name: "index_sessions_on_session_id", unique: true, using: :btree
  add_index "sessions", ["updated_at"], name: "index_sessions_on_updated_at", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "name",               limit: 255,             null: false
    t.string   "email",              limit: 255,             null: false
    t.string   "crypted_password",   limit: 255,             null: false
    t.string   "password_salt",      limit: 255,             null: false
    t.string   "persistence_token",  limit: 255,             null: false
    t.string   "perishable_token",   limit: 255,             null: false
    t.integer  "login_count",        limit: 4,   default: 0, null: false
    t.integer  "failed_login_count", limit: 4,   default: 0, null: false
    t.datetime "last_request_at"
    t.datetime "current_login_at"
    t.datetime "last_login_at"
    t.string   "current_login_ip",   limit: 255
    t.string   "last_login_ip",      limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree

end
