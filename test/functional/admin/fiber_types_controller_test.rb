require 'test_helper'

class Admin::FiberTypesControllerTest < ActionController::TestCase
  def test_index
    get :index
    assert_template 'index'
  end

  def test_show
    get :show, :id => FiberType.first
    assert_template 'show'
  end

  def test_new
    get :new
    assert_template 'new'
  end

  def test_create_invalid
    FiberType.any_instance.stubs(:valid?).returns(false)
    post :create
    assert_template 'new'
  end

  def test_create_valid
    FiberType.any_instance.stubs(:valid?).returns(true)
    post :create
    assert_redirected_to admin_fiber_type_url(assigns(:fiber_type))
  end

  def test_edit
    get :edit, :id => FiberType.first
    assert_template 'edit'
  end

  def test_update_invalid
    FiberType.any_instance.stubs(:valid?).returns(false)
    put :update, :id => FiberType.first
    assert_template 'edit'
  end

  def test_update_valid
    FiberType.any_instance.stubs(:valid?).returns(true)
    put :update, :id => FiberType.first
    assert_redirected_to admin_fiber_type_url(assigns(:fiber_type))
  end

  def test_destroy
    fiber_type = FiberType.first
    delete :destroy, :id => fiber_type
    assert_redirected_to admin_fiber_types_url
    assert !FiberType.exists?(fiber_type.id)
  end
end
