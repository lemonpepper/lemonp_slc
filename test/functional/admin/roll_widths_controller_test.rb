require 'test_helper'

class Admin::RollWidthsControllerTest < ActionController::TestCase
  def test_index
    get :index
    assert_template 'index'
  end

  def test_show
    get :show, :id => RollWidth.first
    assert_template 'show'
  end

  def test_new
    get :new
    assert_template 'new'
  end

  def test_create_invalid
    RollWidth.any_instance.stubs(:valid?).returns(false)
    post :create
    assert_template 'new'
  end

  def test_create_valid
    RollWidth.any_instance.stubs(:valid?).returns(true)
    post :create
    assert_redirected_to admin_roll_width_url(assigns(:roll_width))
  end

  def test_edit
    get :edit, :id => RollWidth.first
    assert_template 'edit'
  end

  def test_update_invalid
    RollWidth.any_instance.stubs(:valid?).returns(false)
    put :update, :id => RollWidth.first
    assert_template 'edit'
  end

  def test_update_valid
    RollWidth.any_instance.stubs(:valid?).returns(true)
    put :update, :id => RollWidth.first
    assert_redirected_to admin_roll_width_url(assigns(:roll_width))
  end

  def test_destroy
    roll_width = RollWidth.first
    delete :destroy, :id => roll_width
    assert_redirected_to admin_roll_widths_url
    assert !RollWidth.exists?(roll_width.id)
  end
end
