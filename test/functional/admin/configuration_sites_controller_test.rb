require 'test_helper'

class Admin::ConfigurationSitesControllerTest < ActionController::TestCase
  def test_index
    get :index
    assert_template 'index'
  end

  def test_show
    get :show, :id => ConfigurationSite.first
    assert_template 'show'
  end

  def test_new
    get :new
    assert_template 'new'
  end

  def test_create_invalid
    ConfigurationSite.any_instance.stubs(:valid?).returns(false)
    post :create
    assert_template 'new'
  end

  def test_create_valid
    ConfigurationSite.any_instance.stubs(:valid?).returns(true)
    post :create
    assert_redirected_to admin_configuration_site_url(assigns(:configuration_site))
  end

  def test_edit
    get :edit, :id => ConfigurationSite.first
    assert_template 'edit'
  end

  def test_update_invalid
    ConfigurationSite.any_instance.stubs(:valid?).returns(false)
    put :update, :id => ConfigurationSite.first
    assert_template 'edit'
  end

  def test_update_valid
    ConfigurationSite.any_instance.stubs(:valid?).returns(true)
    put :update, :id => ConfigurationSite.first
    assert_redirected_to admin_configuration_site_url(assigns(:configuration_site))
  end

  def test_destroy
    configuration_site = ConfigurationSite.first
    delete :destroy, :id => configuration_site
    assert_redirected_to admin_configuration_sites_url
    assert !ConfigurationSite.exists?(configuration_site.id)
  end
end
