require 'test_helper'

class Admin::HeightFibreasControllerTest < ActionController::TestCase
  def test_index
    get :index
    assert_template 'index'
  end

  def test_show
    get :show, :id => HeightFibrea.first
    assert_template 'show'
  end

  def test_new
    get :new
    assert_template 'new'
  end

  def test_create_invalid
    HeightFibrea.any_instance.stubs(:valid?).returns(false)
    post :create
    assert_template 'new'
  end

  def test_create_valid
    HeightFibrea.any_instance.stubs(:valid?).returns(true)
    post :create
    assert_redirected_to admin_height_fibrea_url(assigns(:height_fibrea))
  end

  def test_edit
    get :edit, :id => HeightFibrea.first
    assert_template 'edit'
  end

  def test_update_invalid
    HeightFibrea.any_instance.stubs(:valid?).returns(false)
    put :update, :id => HeightFibrea.first
    assert_template 'edit'
  end

  def test_update_valid
    HeightFibrea.any_instance.stubs(:valid?).returns(true)
    put :update, :id => HeightFibrea.first
    assert_redirected_to admin_height_fibrea_url(assigns(:height_fibrea))
  end

  def test_destroy
    height_fibrea = HeightFibrea.first
    delete :destroy, :id => height_fibrea
    assert_redirected_to admin_height_fibreas_url
    assert !HeightFibrea.exists?(height_fibrea.id)
  end
end
