require 'test_helper'

class Admin::InternalImagesControllerTest < ActionController::TestCase
  def test_index
    get :index
    assert_template 'index'
  end

  def test_show
    get :show, :id => InternalImage.first
    assert_template 'show'
  end

  def test_new
    get :new
    assert_template 'new'
  end

  def test_create_invalid
    InternalImage.any_instance.stubs(:valid?).returns(false)
    post :create
    assert_template 'new'
  end

  def test_create_valid
    InternalImage.any_instance.stubs(:valid?).returns(true)
    post :create
    assert_redirected_to admin_internal_image_url(assigns(:internal_image))
  end

  def test_edit
    get :edit, :id => InternalImage.first
    assert_template 'edit'
  end

  def test_update_invalid
    InternalImage.any_instance.stubs(:valid?).returns(false)
    put :update, :id => InternalImage.first
    assert_template 'edit'
  end

  def test_update_valid
    InternalImage.any_instance.stubs(:valid?).returns(true)
    put :update, :id => InternalImage.first
    assert_redirected_to admin_internal_image_url(assigns(:internal_image))
  end

  def test_destroy
    internal_image = InternalImage.first
    delete :destroy, :id => internal_image
    assert_redirected_to admin_internal_images_url
    assert !InternalImage.exists?(internal_image.id)
  end
end
