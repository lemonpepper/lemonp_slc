class ProductLine < ActiveRecord::Base
  #attr_accessible :name, :order, :active, :published
  has_attached_file :product_line_image, styles: { medium: "300x300>", thumb: "50x50>", otimize: "100%x100%"},processors: [:thumbnail, :compression]
  validates_attachment_content_type :product_line_image, :content_type => /\Aimage\/.*\Z/
  
  has_attached_file :product_line_internal_image, styles: { medium: "300x300>", thumb: "50x50>", otimize: "100%x100%"},processors: [:thumbnail, :compression]
  validates_attachment_content_type :product_line_internal_image, :content_type => /\Aimage\/.*\Z/
  
  has_attached_file :product_line_featured_image, styles: { medium: "300x300>", thumb: "50x50>", otimize: "100%x100%"},processors: [:thumbnail, :compression]
  validates_attachment_content_type :product_line_featured_image, :content_type => /\Aimage\/.*\Z/
  extend FriendlyId
  friendly_id :name, use: :slugged
  
  has_many :products
end
