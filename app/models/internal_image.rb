class InternalImage < ActiveRecord::Base
  #attr_accessible :title, :category
  
  has_attached_file :featured_internal, styles: { medium: "300x300>", thumb: "50x50>", otimize: "100%x100%"},processors: [:thumbnail, :compression]
  validates_attachment_content_type :featured_internal, :content_type => /\Aimage\/.*\Z/

  validates :featured_internal, presence: true
end
