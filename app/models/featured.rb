class Featured < ActiveRecord::Base
  #attr_accessible :active, :published
  has_attached_file :featured_image, styles: { medium: "300x300>", thumb: "50x50>", otimize: "100%x100%" },processors: [:thumbnail, :compression]
  validates_attachment_content_type :featured_image, :content_type => /\Aimage\/.*\Z/

  validates :featured_image, presence: true
end
