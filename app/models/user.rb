class User < ActiveRecord::Base
  acts_as_authorization_subject
  validates_presence_of :name
  validates_presence_of :email
  
  acts_as_authentic do |c|
    c.login_field = "email"
    c.session_class = UserSession
    c.validates_format_of_email_field_options = {:with => Authlogic::Regex.email_nonascii}
  end
  
  def self.find_by_downcase_email(login)
    find_by_email(login.downcase)
  end
  
end
