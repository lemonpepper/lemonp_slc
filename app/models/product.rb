class Product < ActiveRecord::Base
  #attr_accessible :title, :description, :product_line_id, :height_fibrea_id, :roll_width_id, :fiber_type_id, :length_roll, :weight, :thickness_wire, :basis, :warranty

  has_attached_file :product_home_image, styles: { medium: "300x300>", thumb: "51x51#", home: "231x124#", internal: "680x464#"},processors: [:thumbnail, :compression]
  validates_attachment_content_type :product_home_image, :content_type => /\Aimage\/.*\Z/

  validates :product_line_id, presence: true 
  validates :height_fibrea_id, presence: true
  validates :roll_width_id, presence: true
  validates :fiber_type_id, presence: true
  validates :product_home_image, presence: true

  belongs_to :product_line
  belongs_to :height_fibrea
  belongs_to :roll_width
  belongs_to :fiber_type
  
  has_many :product_images
  
  extend FriendlyId
  friendly_id :title, use: :slugged
end
