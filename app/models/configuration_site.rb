class ConfigurationSite < ActiveRecord::Base
  #attr_accessible :title_institutional, :description_institutional, :movie_institutional, :movie_text_institutional, :title_research_innovation, :description_research_innovation, :title_warranty, :description_warranty, :link_facebook, :link_instagram, :email_contact

  has_attached_file :cover_institutional, styles: {otimize: "100%x100%" },processors: [:thumbnail, :compression]
  validates_attachment_content_type :cover_institutional, :content_type => /\Aimage\/.*\Z/

  has_attached_file :cover_synthetic_grass, styles: {otimize: "100%x100%" },processors: [:thumbnail, :compression]
  validates_attachment_content_type :cover_synthetic_grass, :content_type => /\Aimage\/.*\Z/
  
  has_attached_file :cover_research_innovation, styles: {otimize: "100%x100%" },processors: [:thumbnail, :compression]
  validates_attachment_content_type :cover_research_innovation, :content_type => /\Aimage\/.*\Z/
  
  has_attached_file :cover_warranty, styles: {otimize: "100%x100%" },processors: [:thumbnail, :compression]
  validates_attachment_content_type :cover_warranty, :content_type => /\Aimage\/.*\Z/
  
  has_attached_file :icon_institutional, styles: {otimize: "100%x100%" },processors: [:thumbnail, :compression]
  validates_attachment_content_type :icon_institutional, :content_type => /\Aimage\/.*\Z/

  has_attached_file :icon_synthetic_grass, styles: {otimize: "100%x100%" },processors: [:thumbnail, :compression]
  validates_attachment_content_type :icon_synthetic_grass, :content_type => /\Aimage\/.*\Z/

  has_attached_file :icon_research_innovation, styles: {otimize: "100%x100%" },processors: [:thumbnail, :compression]
  validates_attachment_content_type :icon_research_innovation, :content_type => /\Aimage\/.*\Z/
  
  has_attached_file :icon_warranty, styles: {otimize: "100%x100%" },processors: [:thumbnail, :compression]
  validates_attachment_content_type :icon_warranty, :content_type => /\Aimage\/.*\Z/

end
