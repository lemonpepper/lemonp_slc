class ProductImage < ActiveRecord::Base
  #attr_accessible :title, :product_id
  belongs_to :product
  
  has_attached_file :image_prod, styles: { medium: "300x300>", thumb: "51x51#", home: "231x124#", internal: "680x464#", otimize: "100%x100%"},processors: [:thumbnail, :compression]
  validates_attachment_content_type :image_prod, :content_type => /\Aimage\/.*\Z/
  
  validates :image_prod, presence: true
end
