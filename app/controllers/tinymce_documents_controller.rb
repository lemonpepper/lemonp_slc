class TinymceDocumentsController < ApplicationController
  def create
    # Take upload from params[:file] and store it somehow...
    # Optionally also accept params[:hint] and consume if needed
    params.permit!
    document_file = Document.create params.slice(:file)

    render json: {
      document: {
        url: document_file.file.url
      }
    }, layout: false, content_type: "text/html"
  end
end