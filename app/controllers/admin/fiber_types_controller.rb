class Admin::FiberTypesController < ApplicationController
  layout "admin"
  access_control do
      allow logged_in
  end
  def index
    @fiber_types = FiberType.all_active
  end

  def show
    @fiber_type = FiberType.find(params[:id])
  end

  def new
    @fiber_type = FiberType.new
  end

  def create
    params.permit!
    @fiber_type = FiberType.new(params[:fiber_type])
    if @fiber_type.save
      redirect_to [:admin, @fiber_type], :notice => "Successfully created fiber type."
    else
      render :action => 'new'
    end
  end

  def edit
    @fiber_type = FiberType.find(params[:id])
  end

  def update
    params.permit!
    @fiber_type = FiberType.find(params[:id])
    if @fiber_type.update_attributes(params[:fiber_type])
      redirect_to [:admin, @fiber_type], :notice  => "Successfully updated fiber type."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @fiber_type = FiberType.find(params[:id])
    @fiber_type.newdestroy
    redirect_to admin_fiber_types_url, :notice => "Successfully destroyed fiber type."
  end
end
