class Admin::ConfigurationSitesController < ApplicationController
  layout "admin"
  access_control do
      allow logged_in
  end
  def index
    @configuration_sites = ConfigurationSite.all_active
  end

  def show
    @configuration_site = ConfigurationSite.find(params[:id])
  end

  def new
    @configuration_site = ConfigurationSite.new
  end

  def create
    params.permit!
    @configuration_site = ConfigurationSite.new(params[:configuration_site])
    if @configuration_site.save
      redirect_to [:admin, @configuration_site], :notice => "Successfully created configuration site."
    else
      render :action => 'new'
    end
  end

  def edit
    @configuration_site = ConfigurationSite.find(1)
  end

  def update
    params.permit!
    @configuration_site = ConfigurationSite.find(1)
    if @configuration_site.update_attributes(params[:configuration_site])
      redirect_to edit_admin_configuration_site_path("slc"), :notice  => "Successfully updated configuration site."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @configuration_site = ConfigurationSite.find(params[:id])
    @configuration_site.newdestroy
    redirect_to admin_configuration_sites_url, :notice => "Successfully destroyed configuration site."
  end
end
