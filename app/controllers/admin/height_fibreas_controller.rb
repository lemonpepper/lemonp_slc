class Admin::HeightFibreasController < ApplicationController
  layout "admin"
  access_control do
      allow logged_in
  end
  def index
    @height_fibreas = HeightFibrea.all_active
  end

  def show
    @height_fibrea = HeightFibrea.find(params[:id])
  end

  def new
    @height_fibrea = HeightFibrea.new
  end

  def create
    params.permit!
    @height_fibrea = HeightFibrea.new(params[:height_fibrea])
    if @height_fibrea.save
      redirect_to [:admin, @height_fibrea], :notice => "Successfully created height fibrea."
    else
      render :action => 'new'
    end
  end

  def edit
    @height_fibrea = HeightFibrea.find(params[:id])
  end

  def update
    params.permit!
    @height_fibrea = HeightFibrea.find(params[:id])
    if @height_fibrea.update_attributes(params[:height_fibrea])
      redirect_to [:admin, @height_fibrea], :notice  => "Successfully updated height fibrea."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @height_fibrea = HeightFibrea.find(params[:id])
    @height_fibrea.newdestroy
    redirect_to admin_height_fibreas_url, :notice => "Successfully destroyed height fibrea."
  end
end
