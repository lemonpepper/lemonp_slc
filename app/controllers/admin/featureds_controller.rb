class Admin::FeaturedsController < ApplicationController
  layout "admin"
  access_control do
      allow logged_in
  end
  def index
    @featureds = Featured.all_active
  end

  def show
    @featured = Featured.find(params[:id])
  end

  def new
    @featured = Featured.new
  end

  def create
    params.permit!
    @featured = Featured.new(params[:featured])
    if @featured.save
      redirect_to [:admin, @featured], :notice => "Successfully created featured."
    else
      render :action => 'new'
    end
  end

  def edit
    @featured = Featured.find(params[:id])
  end

  def update
    params.permit!
    @featured = Featured.find(params[:id])
    if @featured.update_attributes(params[:featured])
      redirect_to [:admin, @featured], :notice  => "Successfully updated featured."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @featured = Featured.find(params[:id])
    @featured.newdestroy
    redirect_to admin_featureds_url, :notice => "Successfully destroyed featured."
  end
  
  def destroy_image
    params.permit!
    @featured = Featured.find(params[:id])
    @featured.featured_image = nil
    @featured.save
    render :action => 'edit'
  end
end
