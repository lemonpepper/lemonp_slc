class Admin::UsersController < ApplicationController
  layout "admin"
  access_control do
      allow logged_in
  end
  
  def index
    @users = User.all
  end

  def show
    @user = User.find(params[:id])
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(ad_params)
    if @user.save
      redirect_to [:admin, @user], :notice => "Successfully created user."
    else
      render :action => 'new'
    end
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if @user.update_attributes(ad_params)
      redirect_to [:admin, @user], :notice  => "Successfully updated user."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @user = User.find(params[:id])
    @user.destroy
    redirect_to admin_users_url, :notice => "Successfully destroyed user."
  end
  
  
  private

  def ad_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation)
  end
  
end
