class Admin::RollWidthsController < ApplicationController
  layout "admin"
  access_control do
      allow logged_in
  end
  def index
    @roll_widths = RollWidth.all_active
  end

  def show
    @roll_width = RollWidth.find(params[:id])
  end

  def new
    @roll_width = RollWidth.new
  end

  def create
    params.permit!
    @roll_width = RollWidth.new(params[:roll_width])
    if @roll_width.save
      redirect_to [:admin, @roll_width], :notice => "Successfully created roll width."
    else
      render :action => 'new'
    end
  end

  def edit
    @roll_width = RollWidth.find(params[:id])
  end

  def update
    params.permit!
    @roll_width = RollWidth.find(params[:id])
    if @roll_width.update_attributes(params[:roll_width])
      redirect_to [:admin, @roll_width], :notice  => "Successfully updated roll width."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @roll_width = RollWidth.find(params[:id])
    @roll_width.newdestroy
    redirect_to admin_roll_widths_url, :notice => "Successfully destroyed roll width."
  end
end
