class Admin::InternalImagesController < ApplicationController
  layout "admin"
  access_control do
      allow logged_in
  end
  def index
    @internal_images = InternalImage.all_active
  end

  def show
    @internal_image = InternalImage.find(params[:id])
  end

  def new
    @internal_image = InternalImage.new
  end

  def create
    params.permit!
    @internal_image = InternalImage.new(params[:internal_image])
    if @internal_image.save
      redirect_to [:admin, @internal_image], :notice => "Successfully created internal image."
    else
      render :action => 'new'
    end
  end

  def edit
    @internal_image = InternalImage.find(params[:id])
  end

  def update
    params.permit!
    @internal_image = InternalImage.find(params[:id])
    if @internal_image.update_attributes(params[:internal_image])
      redirect_to [:admin, @internal_image], :notice  => "Successfully updated internal image."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @internal_image = InternalImage.find(params[:id])
    @internal_image.newdestroy
    redirect_to admin_internal_images_url, :notice => "Successfully destroyed internal image."
  end
end
