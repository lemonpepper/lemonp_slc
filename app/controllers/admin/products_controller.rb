class Admin::ProductsController < ApplicationController
  layout "admin"
  access_control do
      allow logged_in
  end
  before_filter :load_categories, :only => [:new, :create, :edit, :update]
  def index
    @products = Product.all_active
  end

  def show
    @product = Product.find_by_slug(params[:id])
  end

  def new
    @product = Product.new
  end

  def create
    params.permit!
    @product = Product.new(params[:product])
    if @product.save
      redirect_to [:admin, @product], :notice => "Successfully created product."
    else
      render :action => 'new'
    end
  end

  def edit
    @product = Product.find_by_slug(params[:id])
  end

  def update
    params.permit!
    @product = Product.find_by_slug(params[:id])
    if @product.update_attributes(params[:product])
      redirect_to [:admin, @product], :notice  => "Successfully updated product."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @product = Product.find_by_slug(params[:id])
    @product.newdestroy
    redirect_to admin_products_url, :notice => "Successfully destroyed product."
  end
  
  def load_categories
    @product_lines = ProductLine.all_published.collect {|c| [c.name, c.id]}  
    @height_fibreas = HeightFibrea.all_published.collect {|c| [c.title, c.id]}  
    @roll_widths = RollWidth.all_published.collect {|c| [c.title, c.id]}
    @fiber_types = FiberType.all_published.collect {|c| [c.title, c.id]}
  end
end
