class Admin::ProductLinesController < ApplicationController
  layout "admin"
  access_control do
      allow logged_in
  end
  def index
    @product_lines = ProductLine.all_active
  end

  def show
    @product_line = ProductLine.find_by_slug(params[:id])
  end

  def new
    @product_line = ProductLine.new
  end

  def create
    params.permit!
    @product_line = ProductLine.new(params[:product_line])
    if @product_line.save
      redirect_to admin_product_lines_url, :notice => "Successfully created product line."
    else
      render :action => 'new'
    end
  end

  def edit
    @product_line = ProductLine.find_by_slug(params[:id])
  end

  def update
    params.permit!
    @product_line = ProductLine.find_by_slug(params[:id])
    if @product_line.update_attributes(params[:product_line])
      redirect_to admin_product_lines_url, :notice  => "Successfully updated product line."
    else
      render :action => 'edit'
    end
  end

  def destroy
    @product_line = ProductLine.find_by_slug(params[:id])
    @product_line.newdestroy
    redirect_to admin_product_lines_url, :notice => "Successfully destroyed product line."
  end
end
