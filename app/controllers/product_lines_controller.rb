class ProductLinesController < ApplicationController
  before_filter :load_data
  def show
    @product_line = ProductLine.friendly.find(params[:id])
    @products = @product_line.products.all_published
  end
end
