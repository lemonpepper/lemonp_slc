class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  helper_method :current_user_session, :current_user
  protect_from_forgery with: :exception
  
  def load_data
    @product_lines = ProductLine.all_published
    @configuration_site = ConfigurationSite.find_by_id(1)
  end
  
  private
    def current_user_session
      return @current_user_session if defined?(@current_user_session)
      @current_user_session = UserSession.find
    end

    def current_user
      return @current_user if defined?(@current_user)
      @current_user = current_user_session && current_user_session.user
    end
  
  
  rescue_from 'Acl9::AccessDenied', :with => :access_denied

  def access_denied
    if current_user
      redirect_to login_path, :notice => t("message_show.permission")      
    else
      redirect_to login_path, :notice => t("message_show.to_login")  
    end
  end
  
  
  
  
  protected

  def handle_unverified_request
    # raise an exception
    fail ActionController::InvalidAuthenticityToken
    # or destroy session, redirect
    if current_user_session
      current_user_session.destroy
    end
    redirect_to root_url
  end
    
end
