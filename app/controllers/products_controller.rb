class ProductsController < ApplicationController
  before_filter :load_data
  def show
    @product = Product.where(slug: params[:id], active: true, published: true).first
    if @product.blank?
      redirect_to root_path
    else
      @product_images = @product.product_images.all_published
    end
  end
end
