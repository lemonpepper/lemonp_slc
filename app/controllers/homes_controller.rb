class HomesController < ApplicationController
  
  before_filter :load_data
  before_filter :load_filter, only: [:index, :filters]
  def index
    @featureds = Featured.all_published
    @products = Product.all_published
  end
  
  def about
    @images = show_image(1)
  end
  
  def synthetic_grass
    @images = show_image(2)
  end
  
  def research_and_innovation
    @images = show_image(3)
  end
  
  def warranty
    @images = show_image(4)
  end
  
  def show_image(category_show)
    return InternalImage.where(category: category_show, active: true, published: true)
  end
  
  def filters
    filter_show = false
    filter = "active = 1 and published = 1"
    if !params[:product_line].blank?
      @product_line = ProductLine.where(slug: params[:product_line], active: true, published: true).first
      if !@product_line.blank?
        filter =  filter + " and product_line_id=#{@product_line.id}"
        filter_show = true
      end
    end
    if !params[:height_fibrea].blank?
      @height_fibrea = HeightFibrea.where(id: params[:height_fibrea], active: true, published: true).first
      if !@height_fibrea.blank?
        filter =  filter + " and height_fibrea_id=#{@height_fibrea.id}"
        filter_show = true
      end
    end    
    if !params[:roll_width].blank?
      @roll_width = RollWidth.where(id: params[:roll_width], active: true, published: true).first
      if !@roll_width.blank?
        filter =  filter + " and roll_width_id=#{@roll_width.id}"
        filter_show = true
      end
    end    
    if !params[:fiber_type].blank?
      @fiber_type = FiberType.where(id: params[:fiber_type], active: true, published: true).first
      if !@fiber_type.blank?
        filter =  filter + " and fiber_type_id=#{@fiber_type.id}"
        filter_show = true
      end
    end
    if filter_show
      @products = Product.where(filter)
    end
  end 
  
  def load_filter
    @product_lines_filter = ProductLine.all_published("name")
    @height_fibreas_filter = HeightFibrea.all_published("title")
    @roll_widths_filter = RollWidth.all_published("title")
    @fiber_types_filter = FiberType.all_published("title")
  end
end
